import * as PIXI from "pixi.js";

export enum Direction {
  Up,
  Down,
  Left,
  Right
}

export class TontrisSquare {
  constructor(
    public x: number = 4,
    public y: number = 0,
    public color: number
  ) {}


  generateGraphics(blockSize: number) {
    const graphics = new PIXI.Graphics();
    graphics.beginFill(this.color);
    // set the line style to have a width of 5 and set the color to red
    graphics.lineStyle(0.5, 0xFFFFFF);
    // draw a rectangle
    graphics.drawRect(this.x * blockSize, this.y * blockSize, blockSize, blockSize);
    return graphics

  }
}

const configurations = [
  {
    color: 0x0FA043, //square
    grid: [[1, 1], [2, 1], [1, 2], [2, 2]],
    pivot: [1.5, 1.5]
  },
  {
    color: 0x0FA033, //line
    grid: [[0, 2], [1, 2], [2, 2], [3, 2]],
    pivot: [1.5, 1.5]
  },
  {
    color: 0x0FA0FF, // L shape
    grid: [[0, 0], [0, 1], [1, 1], [2, 1]],
    pivot: [1, 1]
  },
  {
    color: 0x0FA0FF, // other L shape
    grid: [[2, 0], [0, 1], [1, 1], [2, 1]],
    pivot: [1, 1]
  },
  { 
    color: 0x6FAD43, // other z shape 
    grid: [[0, 0], [0, 1], [1, 1], [1, 2]],
    pivot: [1.5, 1.5]
  },
  {
    color: 0x4FA243, //z shape
    grid: [[1, 1], [1, 2], [0, 2], [0, 3]],
    pivot: [1.5, 1.5]
  },
  {
    color: 0xFF00FF, //mountain
    grid: [[0, 1], [0, 2], [0, 3], [1, 2]],
    pivot: [0 , 2]
  }
]

export class TontrisPiece {
  squares: TontrisSquare[]
  pivot: [number, number]

  constructor(
    public x: number,
    public y: number,
    public rotation: number,
    public blockSize: number,
  ) {
    const configIndex = Math.round(Math.random() * (configurations.length - 1 ))
    const config = configurations[configIndex]
    this.squares = config.grid.map(g => new TontrisSquare(g[0], g[1], config.color))
    this.pivot = config.pivot as [number, number]
  }

  generateGraphics(yOffset: number = 0) {
    let container = new PIXI.Container();
    for (let square of this.squares) {
      container.addChild(square.generateGraphics(this.blockSize))
    }
    container.position.x = this.x * this.blockSize
    container.position.y = (yOffset ? yOffset: this.y) * this.blockSize
    return container
  }
}


/*

_|_|X|_|_|_|
_|_|_|_|_|_|
_|_|_|_|_|_|
_|_|_|_|_|_|
_|_|_|_|_|_|
_|_|_|_|_|_|

*/

export class Grid {
  data: number[][]
  constructor(
    public width: number,
    public height: number,
    public blockSize: number,
  ) {
    this.data = []
    for (let x = 0; x < width; x++) {
      this.data[x] = []
    }
  }

  generatePiece(): TontrisPiece {
    return new TontrisPiece(
      Math.round(this.width / 2),
      0,
      0,
      this.blockSize)
  }

  set(x: number, y: number, value: number) {
    this.data[x][y] = value
  }

  get(x: number, y: number) {
    if (x >= this.width || x < 0) {
      return undefined
    }
    return this.data[x][y]
  }

  rotate(piece: TontrisPiece) {
    for (const square of piece.squares) {
      const x = square.x
      const y = square.y
      const newX = piece.pivot[0] + piece.pivot[1] - y
      const newY = piece.pivot[1] - piece.pivot[0] + x

      if (this.get(newX, newY)) {
        return
      }

      square.x = newX
      square.y = newY
    }
  }

  update(piece: TontrisPiece) {
    for (let square of piece.squares) {
      if (piece.y + square.y == this.height - 1) {
        for (let square of piece.squares)
          this.set(piece.x + square.x, piece.y + square.y, square.color)
        return false
      }
      if (this.get(piece.x + square.x, piece.y + square.y + 1)) {
        for (let square of piece.squares)
          this.set(piece.x + square.x, piece.y + square.y, square.color)
        return false
      }
    }
    return true
  }
    
  generateGraphics() {
    const container = new PIXI.Container()
    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        const graphics = new PIXI.Graphics();
        const square = this.get(x, y)
        if (square !== undefined) {
          graphics.beginFill(square );
        } else {
          graphics.beginFill(0x00000);
        }
        // set the line style to have a width of 5 and set the color to red
        graphics.lineStyle(1, 0xFF0000);
        // draw a rectangle
        graphics.drawRect(x * this.blockSize, y * this.blockSize, this.blockSize, this.blockSize);
        container.addChild(graphics)
      }
    }
    return container
  }

  getPreviewGraphic(piece: TontrisPiece) {
    let clonedPiece = JSON.parse(JSON.stringify(piece))
    while (this.allowed(clonedPiece, Direction.Down)) {
      clonedPiece.y++ ;
    }
    return piece.generateGraphics(clonedPiece.y)
  }

  allowed(piece: TontrisPiece, direction: Direction) {
    for (const square of piece.squares) {
      if (direction === Direction.Left) {
        if (piece.x + square.x === 0) {
          return false
        } else if (this.get(piece.x + square.x - 1, piece.y + square.y)) {
          return false
        }
      }
      if (direction === Direction.Right) {
        if (piece.x + square.x === this.width - 1) {
          return false
        } else if (this.get(piece.x + square.x + 1, piece.y + square.y)) {
          return false
        }
      }
      if (direction === Direction.Down) {
        if (piece.y + square.y >= this.height - 1) {
          return false
        } else if (this.get(piece.x + square.x, piece.y + square.y + 1)) {
          return false
        }
      }
    }
    return true
  }

  checkRows() {
    const clonedData = JSON.parse(JSON.stringify(this.data))
    
    const rowsToClear: number[] = []
    for (let y = 0; y < this.height; y++) {
      for (let x = 0; x < this.width; x++) {
        if (!this.get(x, y)) {
          break;
        }
        if (x === this.width - 1) {
          // row needs to be cleared
          for (let yy = 0; yy < y; yy++) {
            // loop throw all rows above
            for (let xx = 0; xx < this.width; xx++) {
              // loop throw column on each row
              const value = clonedData[xx][yy]
              clonedData[xx][yy+1] = value
            }
          }
        }
      }
    }

    // for (let row of rowsToClear) {
    //   for (let y = 0; y < row; y++) {
    //     for (let x = 0; x < this.width; x++) {
    //       const value = this.get(x, y)
    //       clonedData[x][y+1] = value
    //     }
    //   }
    // }
    
    /*
    |_|_|_|
    |x|x|x| 1
    |_|x|_|
    |x|x|x| 3


    |_|_|_|
    |_|_|_| 1
    |_|x|_|
    |x|x|x| 3

    */
    this.data = clonedData
  }
}
