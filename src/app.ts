import Vue from 'vue'
import App from './components/app.vue';
import Store from './store/state'
import Router from './router';

const v = new Vue({
  el: "#app",
  template: '<App/>',
  components: {
    App,
  },
  store: Store,
  router: Router,
});


