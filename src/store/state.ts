// index.ts
import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { RootState } from './types';
import { election } from './election';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
    state: {
        version: '1.0.0', // a simple property,
    } as RootState,
    modules: {
        election
    }
};

export default new Vuex.Store<RootState>(store);