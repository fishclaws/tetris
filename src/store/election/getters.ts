import { ElectionState } from './../types';
import { GetterTree } from 'vuex';
import { RootState } from '../types';

export const getters: GetterTree<ElectionState, RootState> = {
    electionDates: 
        (state: ElectionState) => 
            (usState: string) =>
                state.elections!.filter((e) => {
                    const usStateMatch = e.ocdDivisionId.match(`state:${usState}`);
                    return usStateMatch && usStateMatch[0]
                }).map(e => e.electionDay),

    isLoading:
        (state: ElectionState) => state.elections === undefined
};