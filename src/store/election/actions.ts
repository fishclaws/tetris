import { ElectionState } from './../types';
import { ActionTree } from 'vuex';
import axios from 'axios';
import { Election, RootState } from '../types';

export const actions: ActionTree<ElectionState, RootState> = {
    fetchData({ commit }): any {
        console.log('ACTION');
        axios({
            url: 'https://www.googleapis.com/civicinfo/v2/elections?key=AIzaSyDzYpVtJg7hsdrlXJ-GciyXaAhOE4QdTKA'
        }).then((response) => {
            const payload: Election[] = response && response.data.elections;
            commit('electionLoaded', payload);
        }, (error) => {
            console.log(error);
            commit('electionError', error);
        });
    }
};