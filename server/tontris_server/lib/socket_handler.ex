defmodule TontrisServer.SocketHandler do
  @behaviour :cowboy_websocket

  def init(request, _state) do
    state = %{registry_key: request.path}

    {:cowboy_websocket, request, state}
  end

  def websocket_init(state) do
    Registry.TontrisServer
    |> Registry.register(state.registry_key, {})

    {:ok, state}
  end

  def websocket_handle({:text, json}, state) do
    # payload = Jason.decode!(json)
    # message = payload["data"]["message"]
    # IO.puts(message)
    # Registry.TontrisServer
    # |> Registry.dispatch(state.registry_key, fn(entries) ->
    #   for {pid, _} <- entries do
    #     if pid != self() do
    #       Process.send(pid, message, [])
    #     end
    #   end
    # end)

    # {:reply, {:text, message}, state}
    IO.puts(json)
    {:reply, {:text, json}, state}
  end

  def websocket_info(info, state) do
    {:reply, {:text, info}, state}
  end
end
