defmodule TontrisServer do

  use Application

  def start(_type, _args) do
    children = [
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: TontrisServer.Router,
        options: [
          dispatch: dispatch(),
          port: 4000
        ]
      ),
      Registry.child_spec(
        keys: :duplicate,
        name: Registry.TontrisServer
      )
    ]

    opts = [strategy: :one_for_one, name: TontrisServer.Application]
    Supervisor.start_link(children, opts)
  end

  defp dispatch do
    [
      {:_,
        [
          {"/ws/[...]", TontrisServer.SocketHandler, []},
          {:_, Plug.Cowboy.Handler, {TontrisServer.Router, []}}
        ]
      }
    ]
  end
end
